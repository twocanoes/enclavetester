//
//  EnclaveCommandLine.swift
//  BeyondCorpApp
//
//  Created by Timothy Perfitt on 11/13/20.
//

import Foundation
import CryptoKit


struct EnclaveCommandLine {
 
    var cliOptions:Array<String> = []
    init(withOptions options:Array<String>)  {
        cliOptions = options
        
    }
    func run() {
        
        if cliOptions.contains("-h"){
            usage()
            
        }
        if cliOptions.contains("-c"){
            if let key = generateEnclaveKey(){
                print(key.base64EncodedString())
            }
            else {

                print("ERROR: error creating key")
            }
            
        }
        if cliOptions.contains("-o"){
            let index = cliOptions.firstIndex(of: "-o")
            if let index = index, index < cliOptions.count-1 {
                guard !cliOptions[index+1].starts(with: "-") else
                {
                    print ("ERROR: path not specified")
                    exit(-1)
                }
                
                let filename = cliOptions[index+1]
                guard FileManager.default.fileExists(atPath: filename) else {
                    print ("file does not exist: \(filename)")
                    exit(-1)
                }
                do {
                    let keyPointerString = try String(contentsOfFile: filename)
                    
                    guard let keyData = Data(base64Encoded: keyPointerString, options: .ignoreUnknownCharacters) else {
                        print ("bad key data: \(keyPointerString)")
                        exit(-1)
                    }
                    print("signing..")
                    let privateKeyPointer = try SecureEnclave.P256.Signing.PrivateKey.init(dataRepresentation: keyData)
                    signingTest(key: privateKeyPointer)
                } catch {
                    print("ERROR: file could not be loaded")
                    exit(-1)
                    
                }

                
            }
            else {
                print("ERROR: invalid filename path")
                exit(-1)
            }
            
        }
        else {
            usage()
        }
    }
        
    func generateEnclaveKey() -> Data?{
        if let newKey = try? SecureEnclave.P256.Signing.PrivateKey() {
            return newKey.dataRepresentation
        }
        return nil
    }

    func signingTest(key: SecureEnclave.P256.Signing.PrivateKey)  {
    
       
        let dataToSign = "Test Data".data(using: .utf8)
        print("using reference to secure enclave to sign some data")
        let signature = try? key.signature(for: dataToSign!)
        
        if let signature = signature {
            print("validating signed data")
            let isValid = key.publicKey.isValidSignature(signature, for: dataToSign!)
            if isValid==true{
                print("signing test passed")
            }
            else {
                print("ERROR: validation failed")
            }
            

        }
        else {
            print("ERROR: generating signature failed")
        }

        
    }
        

    func usage()  {
        print("\ncommand line interface to add identity to macOS")
        print("-------------------------------------------------")
        print("-h: this message")
        print("-c: create key in secure enclave and print out in base64 format")
        print("-o <file> : read base64 pointer to private key in secure enclave")
    }

   
}
EnclaveCommandLine(withOptions: CommandLine.arguments).run()


